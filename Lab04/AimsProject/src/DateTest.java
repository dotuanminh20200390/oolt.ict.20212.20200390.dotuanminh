
public class DateTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Initial
		MyDate date2= new MyDate(3,12,2021);
		MyDate date1= new MyDate(3,9,2021);
		//Test the constructor
		MyDate date3= new MyDate(3,"December",2001);
		MyDate[] date= new MyDate[3];
		date[0]=date1;
		date[1]=date2;
		date[2]=date3;
		//Check compare date utils
		DateUtils.dateComparison(date1, date2);
		DateUtils.dateComparison(date3, date2);
		DateUtils.dateComparison(date1, date3);
		
		//Check sort number of dates utils
		DateUtils.dateAscendingSorting(date);
		for (int i=0;i<date.length;++i) {
			System.out.println(date[i].printDate());
		}
		//print the current date in the format the same as �February 29th 2020�
		date1.print();
		
		//print a date with a format chosen by yourself :dd-MMM-yyyy
		date2.printAnotherFormat();
	}

}
