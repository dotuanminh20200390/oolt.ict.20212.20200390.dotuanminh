
public class DateUtils {
	public static void dateComparison(MyDate date1,MyDate date2) {
		if (date1.year<date2.year) {
			System.out.println(date1.printDate()+" begins earlier than "+ date2.printDate());
		}else if (date1.year>date2.year)
			System.out.println(date1.printDate()+" begins later than "+ date2.printDate());
		else {
			if (date1.month<date2.month) {
				System.out.println(date1.printDate()+" begins earlier than "+ date2.printDate());
			}else if (date1.month>date2.month)
				System.out.println(date1.printDate()+" begins later than "+ date2.printDate());
			else {
				if (date1.day<date2.day) {
					System.out.println(date1.printDate()+" begins earlier than "+ date2.printDate());
				}else if (date1.day>date2.day)
					System.out.println(date1.printDate()+" begins later than "+ date2.printDate());
				else {
					System.out.println("These two days are the same");
				}
			}
		}
			
	}
	public static void dateAscendingSorting(MyDate... date) {
		for (int i=0;i<date.length;++i) {
			for (int j=i;j<date.length;++j) {
				if (isEarlierDate(date[j], date[i])== true) {
					MyDate temp = date[i];
					date[i]=date[j];
					date[j]=temp;
				}
			}
		}
	}
	
	public static boolean isEarlierDate(MyDate date1,MyDate date2) {
		if (date1.year<date2.year) {
			return true;
		}else if (date1.year>date2.year)
			return false;
		else {
			if (date1.month<date2.month) {
				return true;
			}else if (date1.month>date2.month)
				return false;
			else {
				if (date1.day<=date2.day) {
					return true;
				}else 
					return false;
			}
		}
			
			
	}
}
