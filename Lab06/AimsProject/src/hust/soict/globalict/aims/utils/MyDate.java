package hust.soict.globalict.aims.utils;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;

public class MyDate {
	public int day ; 
	public int month ;
	public int year;
	public MyDate() {
		LocalDate current_date = LocalDate.now();
		this.day= current_date.getDayOfMonth();
		this.month= current_date.getMonthValue();
		this.year= current_date.getYear();
	}
	public MyDate(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
	}
	public MyDate(int day, String month, int year) {
		this.day = day;
		this.month = checkMonth(month);
		this.year = year;
	}

	
	private int checkMonth(String month) {
		switch(month) {
		case "January":
			return 1;
		case "February":
			return 2;
		case "March":
			return 3;
		case "April":
			return 4;
		case "May":
			return 5;
		case "June":
			return 6;
		case "July":
			return 7;
		case "August":
			return 8;
		case "September":
			return 9;
		case "October":
			return 10;
		case "November":
			return 11;
		case "December":
			return 12;
		}
		return 0;
	}
	public MyDate(String date) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMMM d yyyy", Locale.ENGLISH);
		LocalDate current_date = LocalDate.parse(date, formatter);
		this.day= current_date.getDayOfMonth();
		this.month= current_date.getMonthValue();
		this.year= current_date.getYear();
	}
	public int getDay() {
		return day;
	}
	public void setDay(int day) {
		if(day>=1 && day <=31) {
			this.day = day;
		}else
			System.out.println("Invalid Day");
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		if(month>=1 && month <=12) {
			this.month = month;
		}else
			System.out.println("Invalid Month");
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public MyDate accept() {
		Scanner sc= new Scanner(System.in);
		System.out.println("Enter a date:");
		String date_str =  sc.nextLine();
		MyDate date = new MyDate(date_str);
		return date;
	}
	public void print() {
		//Insert the prefix after the date
		switch(this.day) {
		case 1:
			System.out.println(getMonth(this.month)+" "+ this.day+ "st"+" "+ this.year);
			break;
		case 2:
			System.out.println(getMonth(this.month)+" "+ this.day+ "nd"+" "+ this.year);
			break;
		case 3:
			System.out.println(getMonth(this.month)+" "+ this.day+ "rd"+" "+ this.year);
			break;
		default:
			System.out.println(getMonth(this.month)+" "+ this.day+ "th"+" "+ this.year);
			break;
			
		}
		
		
	}
	public String printDate() {
		return day+"/"+month+"/"+year; 
	}
	public void printAnotherFormat() {
		String string = this.day+"/"+this.month+"/"+this.year;
		  
		Date date1;
		try {
			date1 = new SimpleDateFormat("dd/MM/YYYY").parse(string);
			SimpleDateFormat DateFor = new SimpleDateFormat("dd-MMM-yyyy");
			String stringDate = DateFor.format(date1);
			System.out.println(stringDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
	}
	public String getMonth(int month) {
	    return new DateFormatSymbols().getMonths()[month-1];
	}
	public static void main(String[] args) {
		// Test the constructor, print method
		MyDate date1= new MyDate();
		System.out.println(date1.printDate());
		
		MyDate date2= new MyDate(3,12,2021);
		System.out.println(date2.printDate());
		date2.print();
		date2.printAnotherFormat();
		
		MyDate date3= new MyDate("March 1 2021");
		System.out.println(date3.printDate());
		
		// test the accept function, print method
		MyDate date4= new MyDate();
		date4= date4.accept(); // Enter February 20 2020 for test
		System.out.println(date4.printDate());
		
	}
}


