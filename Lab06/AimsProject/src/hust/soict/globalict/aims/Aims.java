package hust.soict.globalict.aims;

import java.util.ArrayList;
import java.util.Scanner;

import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.order.Order;

public class Aims {

	public static void main(String[] args) {
		showMenu();
	}
	
	public static void showMenu() {

		ArrayList<Order> userOrder = new ArrayList<Order>();
		int choice;
		do{
			System.out.println("Order Management Application: ");
			System.out.println("--------------------------------");
			System.out.println("1. Create new order");
			System.out.println("2. Add item to the order");
			System.out.println("3. Delete item by id");
			System.out.println("4. Display the items list of order");
			System.out.println("0. Exit");
			System.out.println("--------------------------------");
			System.out.println("Please choose a number: 0-1-2-3-4");
	
			Scanner sc = new Scanner(System.in);
			choice = sc.nextInt();
		switch (choice){
			case 1:
				Order newOrder = new Order();
				userOrder.add(newOrder);
				System.out.println("There's a new order with the id: " + newOrder.id);
				break;
			case 2:
				System.out.println("Input the id of the order you want to add items to: ");
				sc = new Scanner(System.in);
				int inputId = sc.nextInt();
				int search = orderSearch(inputId, userOrder);
				if(search == -1){
					System.out.println("Wrong order id!");
					break;
				}

				System.out.println("Input the number of the items you want to add: ");
				sc = new Scanner(System.in);
				int inputNum = sc.nextInt();
				for (int i = 0; i < inputNum; i++) {
					Media newMedia = new Media();
					System.out.println("Enter title: ");
					sc = new Scanner(System.in);
					String title = sc.nextLine();
					System.out.println("Enter category: ");
					sc = new Scanner(System.in);
					String category = sc.nextLine();
					System.out.println("Enter cost: ");
					sc = new Scanner(System.in);
					float cost = sc.nextFloat();
					newMedia.setTitle(title);
					newMedia.setCategory(category);
					newMedia.setCost(cost);
					userOrder.get(search).addMedia(newMedia);
				}
				break;
		

			case 3:
				System.out.println("Input the id of the order you want to delete item from: ");
				sc = new Scanner(System.in);
				int orderInputId = sc.nextInt();
				int orderSearch = orderSearch(orderInputId, userOrder);
				if(orderSearch == -1){
					System.out.println("Wrong order id!");
					break;
				}

				System.out.println("Input the id of the item you want to delete: ");
				sc = new Scanner(System.in);
				int itemId = sc.nextInt();
				userOrder.get(orderSearch).removeMedia(itemId);
				break;

		

			case 4:
				System.out.println("Input the id of the order you want to display: ");
				sc = new Scanner(System.in);
				inputId = sc.nextInt();
				search = orderSearch(inputId, userOrder);
				if(search == -1){
					System.out.println("Wrong order id!");
					break;
				}
				userOrder.get(search).printOrder();
				break;
		

			case 0:
			System.exit(0);
			return;

		}
		}while (choice != 0);	
	}

	public static int orderSearch(int id, ArrayList<Order> userOrder){
		for (int i = 0; i < userOrder.size(); i++) {
            if (userOrder.get(i).id == id)
                return i;
        }
		return -1;
	}
}