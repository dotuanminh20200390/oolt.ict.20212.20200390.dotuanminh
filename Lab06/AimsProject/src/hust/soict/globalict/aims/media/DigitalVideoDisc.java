package hust.soict.globalict.aims.media;
public class DigitalVideoDisc extends Media {
	
	public DigitalVideoDisc(){
		
	}

	public DigitalVideoDisc(String title){
        super(title);
    }

    public DigitalVideoDisc(String title, String category){
        super(title, category);
    }

	private String director;
	private int length;

	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}


	public boolean search(String title){
		int check = 0;
		String[] title_split = title.split(" ");
		for (int i = 0; i < title_split.length; i++)
			if(this.getTitle().toLowerCase().contains(title_split[i].toLowerCase())==false)
				check++;
		if(check == 0)
			return true;
		return false;
	}
}