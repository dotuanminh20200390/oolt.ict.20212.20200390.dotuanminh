package hust.soict.globalict.aims.media;

import java.util.ArrayList;

import hust.soict.globalict.aims.exceptions.PlayerException;

public class CompactDisc extends Disc implements Playable, Comparable {

	private String artist;
	private ArrayList<Track> tracks = new ArrayList<Track>();

	public CompactDisc() {
		super();
	}

	public CompactDisc(String title, String category, float cost, String artist) {
		super(title, category, cost);
		this.artist = artist;
	}

	public void addTrack(Track track) {
		int check = 0;
		for (int i = 0; i < tracks.size(); ++i) {
			if (track.equals(tracks.get(i))) {
				check++;
			}
		}
		if (check == 0) {
			tracks.add(track);
			System.out.println("Your track has been added");
		} else {
			System.out.println("Your track has already in the list");
		}
	}

	public void removeTrack(Track track) {
		int check = 0;
		for (int i = 0; i < tracks.size(); ++i) {
			if (track.getTitle() == tracks.get(i).getTitle()) {
				check++;
			}
		}
		if (check > 0) {
			tracks.remove(track);
			System.out.println("Your track has been removed");
		} else {
			System.out.println("No track founded");
		}
	}

	public int getLength() {
		int sum = 0;
		for (int i = 0; i < tracks.size(); ++i) {
			sum += tracks.get(i).getLength();
		}
		return sum;
	}

	@Override
	public void play() throws PlayerException {
		if (this.getLength() > 0) {
			// TODO Play all tracks in the CD as you have implemented
			java.util.Iterator iter = tracks.iterator();
			Track nextTrack;
			while (iter.hasNext()) {
				nextTrack = (Track) iter.next();
				try {
					nextTrack.play();
				} catch (PlayerException e) {
					throw e;
				}
			}
		} else {
			throw new PlayerException("ERROR: CD length is non-positive!");
		}
	}

	@Override
	public int compareTo(Object obj) {
		// TODO Auto-generated method stub
		if (obj == null) {
			return -1;
		}

		if (!(obj instanceof CompactDisc)) {
			return -1;
		}
		final CompactDisc cd = (CompactDisc) obj;
		if (this.tracks.size() != cd.tracks.size()) {
			return this.tracks.size() > cd.tracks.size() ? 1 : -1;
		} else {
			if (this.getLength() != cd.getLength()) {
				return this.getLength() > cd.getLength() ? 1 : -1;
			}
		}
		return 0;
	}
}
