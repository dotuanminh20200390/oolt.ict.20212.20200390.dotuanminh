import java.util.Scanner;

public class Ex1{
	public static Scanner sc = new Scanner(System.in);
	public static void main(String[] args) {
		System.out.println("Enter the first number");
		Double x1,x2; 
		x1= inputDouble();
		System.out.println("Enter the second number");
		x2=inputDouble();
		System.out.println("The sum of " +x1 + " and "+ x2 + " is "+ (x1+x2));
		System.out.println("The difference of " +x1 + " and "+ x2 + " is "+ (x1-x2));
		System.out.println("The product of " +x1 + " and "+ x2 + " is "+ (x1*x2));
		System.out.println("The quotient of " +x1 + " and "+ x2 + " is "+ (x1/x2));
	}
	public static Double inputDouble() {
		while (true) {
			try {
				return Double.parseDouble(sc.nextLine());
			} catch (Exception e) {
				System.err.println("Error...Reenter:");
			}
		}
	}
}