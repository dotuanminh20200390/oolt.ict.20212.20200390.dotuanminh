public class Order {
	public static final int MAX_NUMBERS_ORDERED =10 ;
	private DigitalVideoDisc itemsOrdered[]= new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
	int qtyOrdered;
	public void addDigitalVideoDisc(DigitalVideoDisc disc) {
		if (qtyOrdered<10) {
			itemsOrdered[qtyOrdered]=disc;
			qtyOrdered++;
			
		}else {
			System.out.println("You have reached your limit in order");
		}
	}
	public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
		int index = 0;
		for (int i=0; i<qtyOrdered;++i) {
			if (itemsOrdered[i].equals(disc)) {
				index=i;
				qtyOrdered--;
			}
		}
		for (int i=index;i< qtyOrdered;++i) {
			itemsOrdered[i]=itemsOrdered[i+1];
		}
		
	}
	
	public void  totalCost() {
		float sum=0;
		for(int i=0; i<qtyOrdered;++i) {
			sum += itemsOrdered[i].getCost();
		}
		System.out.println("Your total cost is "+ sum);
	}

}
