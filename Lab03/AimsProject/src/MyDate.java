import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.Scanner;

public class MyDate {
	public int day ; 
	public int month ;
	public int year;
	public MyDate() {
		LocalDate current_date = LocalDate.now();
		this.day= current_date.getDayOfMonth();
		this.month= current_date.getMonthValue();
		this.year= current_date.getYear();
	}
	public MyDate(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
	}
	
	public MyDate(String date) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMMM d yyyy", Locale.ENGLISH);
		LocalDate current_date = LocalDate.parse(date, formatter);
		this.day= current_date.getDayOfMonth();
		this.month= current_date.getMonthValue();
		this.year= current_date.getYear();
	}
	public int getDay() {
		return day;
	}
	public void setDay(int day) {
		if(day>=1 && day <=31) {
			this.day = day;
		}else
			System.out.println("Invalid Day");
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		if(month>=1 && month <=12) {
			this.month = month;
		}else
			System.out.println("Invalid Month");
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public MyDate accept() {
		Scanner sc= new Scanner(System.in);
		System.out.println("Enter a date:");
		String date_str =  sc.nextLine();
		MyDate date = new MyDate(date_str);
		return date;
	}
	public void print() {
		System.out.println("It is "+day+"/"+month+"/"+year); 
	}
	public static void main(String[] args) {
		// Test the constructor, print method
		MyDate date1= new MyDate();
		date1.print();
		
		MyDate date2= new MyDate(3,12,2021);
		date2.print();
		
		MyDate date3= new MyDate("March 25 2021");
		date3.print();
		
		// test the accept function, print method
		MyDate date4= new MyDate();
		date4= date4.accept(); // Enter February 20 2020 for test
		date4.print();
	}
}


