import java.util.Scanner;

public class displayNumberInMonth {

	public static void main(String[] args) {
		Scanner sc= new Scanner(System.in);
		System.out.println("Enter the month:");
		String strmonth =sc.nextLine();
		int month =monthProcess(strmonth);
		if (month==0)
			System.out.println("Error");
		
		System.out.println("Enter the year (non-negative number):");
		int year =sc.nextInt();
		while(year <0) {
			System.out.println("Wrong...ReEnter:");
			year =sc.nextInt();
		}
		printDaysInMonth(year, month);
	}
	
	public static int monthProcess(String s) {
		int month;
		if (s.equals("January")||s.equals("Jan.")||s.equals("Jan")||s.equals("1") )
			month =1;
		else if(s.equals("February")||s.equals("Feb.")||s.equals("Feb")||s.equals("2") )
			month=2;
		else if(s.equals("March")||s.equals("Mar.")||s.equals("Mar")||s.equals("3") )
			month=3;
		else if(s.equals("April")||s.equals("Apr.")||s.equals("Apr")||s.equals("4") )
			month=4;
		else if(s.equals("May")||s.equals("May")||s.equals("May")||s.equals("5") )
			month=5;
		else if(s.equals("June")||s.equals("June")||s.equals("Jun")||s.equals("6") )
			month=6;
		else if(s.equals("July")||s.equals("July")||s.equals("Jul")||s.equals("7") )
			month=7;
		else if(s.equals("August")||s.equals("Aug.")||s.equals("Aug")||s.equals("8") )
			month=8;
		else if(s.equals("September")||s.equals("Sept.")||s.equals("Sep")||s.equals("9") )
			month=9;
		else if(s.equals("October")||s.equals("Oct.")||s.equals("Oct")||s.equals("10") )
			month=10;
		else if(s.equals("November")||s.equals("Nov.")||s.equals("Nov")||s.equals("11") )
			month=11;
		else if(s.equals("November")||s.equals("Nov.")||s.equals("Nov")||s.equals("11") )
			month=11;
		else if(s.equals("December")||s.equals("Dec.")||s.equals("Dec")||s.equals("12") )
			month=12;
		else
			month = 0; 
		
		return month;
		
	}
	
	public static boolean isLeapYear(int year) {
		if (year %4 ==0) {
			if (year %100 ==0){
				if (year %400== 0) return true;
				else return false;
			}
			else return true;
		}
		else return false;
	}
	
	public static void printDaysInMonth(int year, int month) {
		if (month != 2) {
			if (month ==1 || month ==3|| month ==5|| month ==7|| month ==8|| month ==10|| month ==12)
				System.out.println("This month has 31 days");
			else 
				System.out.println("This month has 30 days");
		}else {
			if (isLeapYear(year)==true)
				System.out.println("This month has 29 days");
			else
				System.out.println("This month has 28 days");
		}
	}
}
