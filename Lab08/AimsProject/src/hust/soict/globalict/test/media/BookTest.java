package hust.soict.globalict.test.media;

import hust.soict.globalict.aims.media.Book;

public class BookTest {

	public static void main(String[] args) {
		Book book1= new Book("Sans famille","Novel",10f);
		book1.addAuthor("Hector Malot");
		book1.setContent("When they are in Toulouse, Vitalis is put in jail \n"
				+ "after an incident with a policeman who is rough with Remi. \n"
				+ "It is not easy for a ten-year-old to feed himself and four \n"
				+ "animals under his care, and they nearly starve, when they \n"
				+ "meet the \"Swan\" - a little river ship owned by Mrs. Milligan \n"
				+ "and her ill son Arthur. Remi is taken in to entertain the sick boy,\n"
				+ "and he becomes almost part of the family. They travel towards Montpellier \n"
				+ "and the Mediterranean on the Canal du Sud. Remi learns the story about her \n"
				+ "dead husband and brother-in-law, who under the English law, was to inherit all \n"
				+ "of his brother's fortune if he died childless. An earlier child had disappeared \n"
				+ "and was never found (under the charge of this James Milligan) however soon after \n"
				+ "the husband's death, Arthur was born.");
		System.out.println(book1.toString());

	}

}
