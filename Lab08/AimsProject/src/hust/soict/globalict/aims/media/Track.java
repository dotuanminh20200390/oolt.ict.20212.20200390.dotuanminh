package hust.soict.globalict.aims.media;

public class Track implements Playable,Comparable{
	private String title;
	private int length;
	public String getTitle() {
		return title;
	}
	public int getLength() {
		return length;
	}
	public Track(String title, int length) {
		this.title = title;
		this.length = length;
	}
	@Override
	public void play() {
		// TODO Auto-generated method stub
		System.out.println("Playing Track: " + this.getTitle());
		System.out.println("Track length: " + this.getLength());
	}
	@Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (!(obj instanceof Track)) {
            return false;
        }

        final Track track = (Track) obj;
        if ((this.title == null) ? (track.title != null) : !this.title.equals(track.title)) {
            return false;
        }
        if (this.length != track.length) {
            return false;
        }

        return true;
    }
	@Override
	public int compareTo(Object obj) {
		// TODO Auto-generated method stub
		if (obj == null) {
            return -1;
        }

        if (!(obj instanceof Track)) {
            return -1;
        }

        final Track track = (Track) obj;
        if ((this.title == null) ? (track.title != null) : !this.title.equals(track.title)) {
            return this.title.compareTo(track.title);
        }
        if (this.length != track.length) {
        	return this.length > track.length ? 1 : -1;
        }

		return 0;
	}
	
	
}
