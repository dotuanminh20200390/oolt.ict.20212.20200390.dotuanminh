package hust.soict.globalict.aims.media;

public class DigitalVideoDisc extends Disc implements Playable,Comparable{
	
	private String director;
	private int length;
 
	public DigitalVideoDisc() {
		super();
	}
	

	public DigitalVideoDisc(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	
	public DigitalVideoDisc(String title, String category, float cost,String director, int length) {
		super(title, category, cost);
		this.director= director;
		this.length= length;
	}

	public String getDirector() {
		return director;
	}
	
	public int getLength() {
		return length;
	}
	

	public boolean search(String title){
		int check = 0;
		String[] title_split = title.split(" ");
		for (int i = 0; i < title_split.length; i++)
			if(this.getTitle().toLowerCase().contains(title_split[i].toLowerCase())==false)
				check++;
		if(check == 0)
			return true;
		return false;
	}

	@Override
	public void play() {
		// TODO Auto-generated method stub
		System.out.println("Playing DVD: " + this.getTitle());
		System.out.println("DVD length: " + this.getLength());
	}
	@Override
	public int compareTo(Object obj) {
		// TODO Auto-generated method stub
		if (obj == null) {
            return -1;
        }

        if (!(obj instanceof DigitalVideoDisc)) {
            return -1;
        }
        final Media dvd = (Media) obj;
        if (this.getCost() != dvd.getCost()) {
            return this.getCost()>dvd.getCost() ? 1 : -1;
        }
		return 0;
	}
}