package hust.soict.globalict.lab01;

import java.util.Scanner;

public class Ex2{
	public static Scanner sc= new Scanner(System.in);
	public static void main(String[] args) {
		while (true) {

			System.out.println("\t\t --- MENU --- \t\t");
			System.out.println("1. Solve the first-degree equation (linear equation) with one variable ");
			System.out.println("2. Solve the system of first-degree equations (linear system) with two variables");
			System.out.println("3. Solve the second-degree equation with one variable");
			System.out.println("4. Exit");
			System.out.print("Enter selection : ");
			int i = inputIntPositive();
			switch (i) {
			case 1:
				firstDegreeEquationOneVariable();
				break;
			case 2:
				firstDegreeEquationTwoVariable();
				break;
			case 3:
				secondDegreeEquationOneVariable();
				break;
			case 4:
				System.exit(i);
				return;
			default:
				System.err.println("Error...Reenter !!!");
				break;
				}
			}
	}
	public static void firstDegreeEquationOneVariable() {
		//Giai phuong trinh bac 1 
		//Nhap a,b 
		Double a,b; 
		System.out.println("Nhap a:");
		a= inputDouble();
		System.out.println("Nhap b:");
		b= inputDouble();
		if (a==0 && b==0) 
			System.out.println("Bai toan vo so nghiem");
		else if (b!=0 && a== 0)
			System.out.println("Phuong trinh vo nghiem");
		else if(a!=0)
			System.out.println("x= "+ (-b/a));
	}
	public static void firstDegreeEquationTwoVariable() {
		Double a11,a12,a21,a22,b1,b2; 
		System.out.println("Nhap a11:");
		a11= inputDouble();
		System.out.println("Nhap a12:");
		a12= inputDouble();
		System.out.println("Nhap a21:");
		a21= inputDouble();
		System.out.println("Nhap a22:");
		a22= inputDouble();
		System.out.println("Nhap b1:");
		b1= inputDouble();
		System.out.println("Nhap b2:");
		b2= inputDouble();
		double D = a11*a22- a12*a21;
		double D1= b1*a22-b2*a12;
		double D2= a11*b2-a21*b1; 
		if (D !=0)
			System.out.println("x1 = " + D1/D + " ,x2= "+ D2/D);
		else if (D1==0 && D2==0 && D==0)
			System.out.println("Pt vo so nghiem");
		else
			System.out.println("Phuong trinh vo nghiem");
	}
	public static void secondDegreeEquationOneVariable() {
		//Giai phuong trinh bac 2
		//Nhap a,b 
		Double a,b,c; 
		System.out.println("Nhap a:");
		a= inputDouble();
		System.out.println("Nhap b:");
		b= inputDouble();
		System.out.println("Nhap c:");
		c= inputDouble();
		
		double delta = b*b -4*a*c ;
		if (delta ==0)
			System.out.println("Phuong trinh co nghiem kep x= "+ -b/(2*a));
		else if (delta >0)
			System.out.println("Phuong trinh co 2 nghiem phan biet x1= "+ (-b+Math.sqrt(delta))/(2*a) +", x2= "+ (-b-Math.sqrt(delta))/(2*a));
		else 
			System.out.println("Phuong trinh vo nghiem");
	}
	public static Double inputDouble() {
		while (true) {
			try {
				return Double.parseDouble(sc.nextLine());
			} catch (Exception e) {
				System.err.println("Error...Reenter:");
			}
		}
	}
	public static int inputIntPositive() {
		while (true) {
			try {
				int intPositive = Integer.parseInt(sc.nextLine());
				if (intPositive >= 0) {
					return intPositive;
				} else {
					System.err.println("Nhập lại:");
				}

			} catch (Exception e) {
				System.err.println("Nhập lại:");
			}

		}
	}
}