package hust.soict.globalict.lab02;


public class ArraySort {

	public static void main(String[] args) {
		int[] a= {1,2,1000,45};
		for (int i=0;i<a.length; ++i) {
			for (int j=0;j<i;++j) {
				if (a[j]>a[i]) {
					int temp; 
					temp=a[i];
					a[i]=a[j];
					a[j]=temp;
				}
			}
		}
		
		int sum=0;
		for (int i=0;i<a.length; ++i) {
			sum=sum+a[i];
			System.out.println(a[i]);
		}
		System.out.println("The sum is "+ sum +", The average is "+sum/(a.length));
		
		
	}

}
