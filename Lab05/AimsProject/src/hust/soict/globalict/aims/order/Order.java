package hust.soict.globalict.aims.order;

import hust.soict.globalict.aims.disc.DigitalVideoDisc;
import hust.soict.globalict.aims.utils.MyDate;

public class Order {
	public static final int MAX_NUMBERS_ORDERED =10 ;
	private DigitalVideoDisc itemsOrdered[]= new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
	int qtyOrdered;
	private MyDate dateOrdered;
	public static final int MAX_LIMITEDS_ORDERS =5 ;
	private static int nbOrders=0;
	public static int luckyNumber=-1;
	public Order() {
		MyDate date= new MyDate();
		this.dateOrdered=date;
		if(nbOrders<MAX_LIMITEDS_ORDERS)
			nbOrders++;
		else 
			System.out.println("You can not add any order due to limitation reached");
		
	}
	public void addDigitalVideoDisc(DigitalVideoDisc disc) {
		if (qtyOrdered<MAX_NUMBERS_ORDERED) {
			itemsOrdered[qtyOrdered]=disc;
			qtyOrdered++;
			
		}else {
			System.out.println("You have reached your limit in order");
		}
	}
//	public void addDigitalVideoDisc(DigitalVideoDisc []dvdList) {
//		if (qtyOrdered+dvdList.length<=MAX_NUMBERS_ORDERED) {
//			for (int i=0;i<dvdList.length;++i) {
//				itemsOrdered[qtyOrdered]=dvdList[i];
//				qtyOrdered++;
//			}
//		}else {
//			System.out.println("You have reached your limit in order");
//		}
//	}
	public void addDigitalVideoDisc(DigitalVideoDisc...dvdList) {
		if (qtyOrdered+dvdList.length<=MAX_NUMBERS_ORDERED) {
			for (int i=0;i<dvdList.length;++i) {
				itemsOrdered[qtyOrdered]=dvdList[i];
				qtyOrdered++;
			}
		}else {
			System.out.println("You have reached your limit in order");
		}
	}
	public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
		int index = 0;
		for (int i=0; i<qtyOrdered;++i) {
			if (itemsOrdered[i].equals(disc)) {
				index=i;
				qtyOrdered--;
			}
		}
		for (int i=index;i< qtyOrdered;++i) {
			itemsOrdered[i]=itemsOrdered[i+1];
		}
		
	}
	
	public void addDigitalVideoDisc(DigitalVideoDisc dvd1,DigitalVideoDisc dvd2) {
		if (qtyOrdered+2<=MAX_NUMBERS_ORDERED) {
			itemsOrdered[qtyOrdered]=dvd1;
			qtyOrdered++;
			itemsOrdered[qtyOrdered]=dvd2;
			qtyOrdered++;
		}else if (qtyOrdered+1==MAX_NUMBERS_ORDERED){
			itemsOrdered[qtyOrdered]=dvd1;
			qtyOrdered++;
			System.out.println("Can not add dvd 2 due to limit in order");
		}else {
			System.out.println("Noone of them can be added.You have reached your limit in order");
		}
	}
	
	public void  totalCost() {
		float sum=0;
		for(int i=0; i<qtyOrdered;++i) {
			if (i!= luckyNumber) {
				sum += itemsOrdered[i].getCost();
			}
		}
		System.out.println("Your total cost is "+ sum+"$");
	}
	public DigitalVideoDisc getItemsOrdered(int i) {
		return itemsOrdered[i];
	}

	public void printOrder() {
		System.out.println("*************************Order*************************************************************");
		System.out.println("Date[" +dateOrdered.printDate()+ "]");
		System.out.println("Ordered Items:");
		for (int i=0;i<qtyOrdered;++i) {
			if (i!= luckyNumber) {
				System.out.println((i+1) + ". ["+ itemsOrdered[i].getTitle()+"] - ["+itemsOrdered[i].getCategory()+"] - ["+itemsOrdered[i].getDirector()+"] - ["+itemsOrdered[i].getLength()+"] - ["+itemsOrdered[i].getCost()+"] $");
			}else {
				System.out.println((i+1) + ". ["+ itemsOrdered[i].getTitle()+"] - ["+itemsOrdered[i].getCategory()+"] - ["+itemsOrdered[i].getDirector()+"] - ["+itemsOrdered[i].getLength()+"] - [0] $ (Special Discount)");
			}
		}
		System.out.println("*******************************************************************************************");
	}
	public MyDate getDateOrdered() {
		return dateOrdered;
	}
	public void setDateOrdered(MyDate dateOrdered) {
		this.dateOrdered = dateOrdered;
	}
	public DigitalVideoDisc getALuckyItem() {
		luckyNumber= (int)(Math.random()*qtyOrdered);
		return itemsOrdered[luckyNumber];
		}
	}
